<?php

namespace AppEntity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrabajoColision
 *
 * @ORM\Table(name="trabajo_colision", indexes={@ORM\Index(name="FK_trabajo_colision_vehiculos", columns={"vehiculo_id"})})
 * @ORM\Entity
 */
class TrabajoColision
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha_ingreso", type="string", length=50, nullable=false)
     */
    private $fechaIngreso;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha_salida", type="string", length=50, nullable=false)
     */
    private $fechaSalida;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", length=100, nullable=false)
     */
    private $observacion;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=10, nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="tiempo", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $tiempo;

    /**
     * @var \Vehiculos
     *
     * @ORM\ManyToOne(targetEntity="Vehiculos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vehiculo_id", referencedColumnName="id")
     * })
     */
    private $vehiculo;


}

<?php

namespace AppEntity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registros
 *
 * @ORM\Table(name="registros", indexes={@ORM\Index(name="FK_registros_colision", columns={"colision_id"}), @ORM\Index(name="FK_registros_trabajo_colision", columns={"trabajo_id"})})
 * @ORM\Entity
 */
class Registros
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha", type="string", length=50, nullable=false)
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="tecnico", type="integer", nullable=false)
     */
    private $tecnico;

    /**
     * @var string
     *
     * @ORM\Column(name="observacion", type="string", length=100, nullable=false)
     */
    private $observacion;

    /**
     * @var \Colision
     *
     * @ORM\ManyToOne(targetEntity="Colision")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colision_id", referencedColumnName="id")
     * })
     */
    private $colision;

    /**
     * @var \TrabajoColision
     *
     * @ORM\ManyToOne(targetEntity="TrabajoColision")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="trabajo_id", referencedColumnName="id")
     * })
     */
    private $trabajo;


}
